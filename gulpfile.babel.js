'use strict';

import gulp from 'gulp';
import concat from 'gulp-concat';
import imagemin from 'gulp-imagemin';
import pngquant from 'imagemin-pngquant';
import plumber from 'gulp-plumber';
import pug from 'gulp-pug';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify';
import svgSprite from 'gulp-svg-sprite';
import cheerio from 'gulp-cheerio';
import webpackStream from 'webpack-stream';
import webpack from "webpack";
import yargs from 'yargs';
import gulpif from 'gulp-if';
import autoprefixer from 'gulp-autoprefixer';
import mincss from 'gulp-clean-css';
import purgeSourcemaps from 'gulp-purge-sourcemaps';
import del from 'del';

let argv = yargs.argv,
    production = !!argv.production;

const
// Путь к используемым файлам и папкам
    cmsTpl = 'public_html/',

    // Массив svg которые не нужно форматировать
    svgIgnore = ['logo.svg','plan-house.svg'],

    paths = {
        watch: {
            pug: './app/pug/**/*.pug',
            scss: [
                './app/scss/**/*.scss',
                './app/scss/*.scss'
            ],
            fonts: './app/scss/fonts/*',
            js: [
                './app/js/**/*/**/*/*.js',
                './app/js/**/*/**/*.js',
                './app/js/**/*/*.js',
                './app/js/**/*.js',
                './app/js/*.js'
            ],
            svg: './app/materials/svg/*.svg',
            img: './app/materials/images/**/*'
        },
        dist: {
            html: './' + cmsTpl,
            css: './' + cmsTpl + 'css',
            js: './' + cmsTpl + 'js',
            img: './' + cmsTpl + 'images',
            svg: './' + cmsTpl + 'images/svg',
        },
        app: {
            common: {
                html: './app/pug/pages/*.pug',
                scss: './app/scss/app.scss',
                font: './app/scss/font.scss',
                fonts: './app/scss/fonts/*',
                js: './app/js/app.js',
                img: [
                    './app/materials/images/**/*.{jpg,jpeg,png,svg}',
                    './app/materials/images/*.{jpg,jpeg,png,svg}'
                ],
                svg: './app/materials/svg/*.svg',
            },
            vendor: {
                js: [
                    './node_modules/swiper/swiper-bundle.min.js',
                    './node_modules/jquery/dist/jquery.min.js',
                    './node_modules/inputmask/dist/jquery.inputmask.min.js',
                    './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
                    './node_modules/print-js/dist/print.js',
                ],
                css: [
                    './node_modules/normalize.css/normalize.css',
                    './node_modules/bootstrap/dist/css/bootstrap.min.css',
                    './node_modules/swiper/swiper-bundle.min.css',
                    './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css',
                ]
            }
        },
    },

// Подключение Browsersync
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload;

function clean() {
    argv.production = true;
    production = true;
    return del([`./${cmsTpl}*`]);
}

function fonts() {
    return gulp.src(paths.app.common.fonts)
        .pipe(gulp.dest(paths.dist.css + '/fonts'));
}

function bundleCSS() {
    return gulp.src([paths.app.common.font, paths.dist.css + '/vendor.min.css', paths.dist.css + '/common.min.css'])
        .pipe(concat('bundle.min.css'))
        .pipe(gulp.dest(paths.dist.css));
}

function bundleJS() {
    return gulp.src([paths.dist.js + '/vendor.min.js', paths.dist.js + '/common.min.js'])
        .pipe(concat('bundle.min.js'))
        .pipe(gulp.dest(paths.dist.js));
}

function cleanRubbish() {
    return del([
        paths.dist.css + '/vendor.min.css', paths.dist.css + '/common.min.css',
        paths.dist.js + '/vendor.min.js', paths.dist.js + '/common.min.js'
    ]);
}

// Для работы Browsersync, автообновление браузера
function serve() {
    if (production) return true;
    browserSync.init({
        server: {
            baseDir: paths.dist.html,
            index: 'index.html'
        }
    });
    gulp.watch(paths.watch.pug).on('change', function (file) {
        if (~file.indexOf('layouts') || ~file.indexOf('components')) html();
        else html('./' + file.replace(/\\/g, "/"));
    });
    gulp.watch(paths.watch.img).on('all', function (action, file) {
        if (action === 'unlink') return;
        img('./' + file.replace(/\\/g, "/"));
    });
    gulp.watch(paths.watch.fonts, gulp.series('fonts'));
    gulp.watch(paths.watch.scss, gulp.series('cssCommon'));
    gulp.watch(paths.watch.js, gulp.series('jsCommon'));
    gulp.watch(paths.watch.svg, gulp.series('spritesSvg'));
    gulp.watch(paths.dist.html + '/*.html').on('change', reload);
}

// Для работы Pug, преобразование Pug в HTML
function html(file) {
    return gulp.src(typeof file === 'string' ? file : paths.app.common.html)
        .pipe(plumber())
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest(paths.dist.html))
        .pipe(gulpif(!production, browserSync.stream()));
}

// Для преобразования Stylus-файлов в CSS
function cssCommon() {
    const array = [];
    if (!production) {
        array.push(paths.app.common.font);
    }
    array.push(paths.app.common.scss);
    return gulp.src(array)
        .pipe(gulpif(!production, sourcemaps.init()))
        .pipe(plumber())
        .pipe(sass({
            outputStyle: production ? 'compressed' : 'nested'
        }).on('error', sass.logError))
        .pipe(gulpif(production, autoprefixer({
            cascade: false,
            grid: true
        })))
        .pipe(gulpif(production, mincss({
            compatibility: "ie8", level: {
                1: {
                    specialComments: 0,
                    removeEmpty: true,
                    removeWhitespace: true
                },
                2: {
                    mergeMedia: true,
                    removeEmpty: true,
                    removeDuplicateFontRules: true,
                    removeDuplicateMediaBlocks: true,
                    removeDuplicateRules: true,
                    removeUnusedAtRules: false
                }
            }
        })))
        .pipe(concat('common.min.css'))
        .pipe(gulpif(!production, sourcemaps.write('.')))
        .pipe(gulp.dest(paths.dist.css))
        .pipe(gulpif(!production, browserSync.stream()));
}

function jsCommon() {
    return gulp.src(paths.app.common.js)
        .pipe(plumber())
        .pipe(gulpif(!production, sourcemaps.init()))
        .pipe(webpackStream({
            mode: production ? 'production' : 'development',
            output: {
                filename: 'common.min.js',
            },
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        exclude: /\.node_modules$/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env'],
                                plugins: ['@babel/plugin-transform-runtime']
                            }
                        }
                    }
                ]
            },

        }), webpack)
        .pipe(uglify())
        .pipe(gulpif(!production, sourcemaps.write('.')))
        .pipe(gulp.dest(paths.dist.js))
        .pipe(gulpif(!production, browserSync.stream()));
}

function cssVendor() {
    return gulp.src(paths.app.vendor.css)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(purgeSourcemaps())
        .pipe(gulpif(production, mincss({
            compatibility: "ie8", level: {
                1: {
                    specialComments: 0,
                    removeEmpty: true,
                    removeWhitespace: true
                },
                2: {
                    mergeMedia: true,
                    removeEmpty: true,
                    removeDuplicateFontRules: true,
                    removeDuplicateMediaBlocks: true,
                    removeDuplicateRules: true,
                    removeUnusedAtRules: false
                }
            }
        })))
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest(paths.dist.css));
}

function jsVendor() {
    return gulp.src(paths.app.vendor.js)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(purgeSourcemaps())
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest(paths.dist.js));
}

function spritesSvg() {
    return gulp.src(paths.app.common.svg)
        .pipe(cheerio({
            run: function ($, file) {
                var status = true,
                    path = file.path.split('\\'),
                    filename = path[path.length - 1];
                svgIgnore.forEach(function (item) {
                    if (!item.indexOf(filename)) {
                        status = false;
                    }
                });
                if (status) {
                    $('style').remove();
                    $('[fill]').removeAttr('fill');
                    $('[style]').removeAttr('style');
                    $('[stroke]').removeAttr('stroke');
                    $('[class]').removeAttr('class');
                }
            },
            parserOptions: {xmlMode: true}
        }))
        .pipe(svgSprite({
            mode: {
                inline: true,
                symbol: {
                    sprite: '../sprite.svg'
                }
            }
        }))
        .pipe(gulp.dest(paths.dist.svg));
}

function img(image) {
    const images = (typeof (image) === 'string') ? image : paths.app.common.img;
    return gulp.src(images)
        .pipe(imagemin([
            imagemin.mozjpeg({quality: 85, progressive: true}),
            imagemin.optipng({optimizationLevel: 7}),
            pngquant({quality: [0.8, 0.85]})
        ]))
        .pipe(gulp.dest(paths.dist.img));
}

exports.clean = clean;
exports.bundleCSS = bundleCSS;
exports.bundleJS = bundleJS;
exports.cssCommon = cssCommon;
exports.html = html;
exports.cssVendor = cssVendor;
exports.jsVendor = jsVendor;
exports.jsCommon = jsCommon;
exports.spritesSvg = spritesSvg;
exports.img = img;
exports.serve = serve;
exports.fonts = fonts;

gulp.task('default', gulp.series(
    gulp.parallel(html, cssVendor, cssCommon, jsVendor, jsCommon, spritesSvg, img, fonts, serve)
));

gulp.task('prod', gulp.series(clean,
    gulp.parallel(html, cssVendor, fonts, cssCommon, jsVendor, jsCommon, spritesSvg, img, fonts),
    gulp.parallel(bundleCSS, bundleJS),
    cleanRubbish
));