import { getFlat } from '../pages/data/select';
import { getCurrentSectionName } from '../pages/data/select';

let data = {};
const images = {
    furnished: '',
    withoutFurniture: '',
    functional: ''
};
let currentImageType = '';

$(document).on('click', '.flat__left__bottom__item:not(.flat__left__bottom__item--custom)', function () {
    $('.flat__left__bottom__item--active').removeClass('flat__left__bottom__item--active');
    const $self = $(this);
    $self.addClass('flat__left__bottom__item--active');
    const imageType = $self.attr('data-type');
    const $imgContainer = $('.flat__image');
    $('img', $imgContainer).attr('src', images[imageType]);
    currentImageType = imageType;
    if (imageType === 'functional') {
        $imgContainer.prepend(`<div class="flat__image__bottom">
            <a class="btn btn-orange" href="${images[imageType]}" download>Скачать планировку</a>
            <a class="flat__image__bottom__what" href="/plan.html" href="${images[imageType]}">Что такое функциональная <br> планировка?</a>
        </div>`);
    } else {
        $('.flat__image__bottom', $imgContainer).remove();
    }
});

$(document).on('click', '.flat__print:not(.flat__print--custom):not(.flat__print--no-icon)', () => {
    window.printJS({
        printable: images[currentImageType],
        type: 'image',
        imageStyle: 'max-width:100%;max-height:calc(80vh - 30px - 20px - 25px);',
        header: `<div style="height: 20vh; margin-bottom: 30px;">
            <div style="font-family: 'Intro', sans-serif; margin-bottom: 20px; font-size: 24px">Квартира №${data.number}</div>
            <div style="display: flex; justify-content: space-between; flex-wrap: wrap;">
                <div style="width: 50%; flex-shrink: 0; font-family: 'Roboto', sans-serif; margin-bottom: 5px; font-size: 18px">Секция ${getCurrentSectionName()}</div>
                <div style="width: 50%; flex-shrink: 0; font-family: 'Roboto', sans-serif; margin-bottom: 5px; font-size: 18px">Этаж ${data.storey_number}</div>
                <div style="width: 50%; flex-shrink: 0; font-family: 'Roboto', sans-serif; margin-bottom: 5px; font-size: 18px">Комнат ${getRooms(data.rooms_number, data.smart_planning)}</div>
                <div style="width: 50%; flex-shrink: 0; font-family: 'Roboto', sans-serif; margin-bottom: 5px; font-size: 18px">Общая, м² ${data.total_area}</div>
            </div>
            <div style="width: 50%; flex-shrink: 0; font-family: 'Intro', sans-serif; margin-top: 15px; font-size: 18px">Цена, ₽ ${(+data.price).toLocaleString('ru-RU',{minimumFractionDigits:0})}</div>
        </div>
        `,
    });
});

$(document).on('click', '.flat__open-image, .flat__image img', () => {
    $.fancybox.open({
        src: images[currentImageType],
        baseClass: 'fancy-white'
    })
});

function openFlat() {
    const $self = $(this);
    const area = $self.attr('data-area');
    const storey = Number($self.attr('data-storey'));
    data = getFlat(storey, area);
    images.furnished = data.images[0];
    images.withoutFurniture = data.images[1];
    images.functional = data.functional_plan;
    $.fancybox.open(`<div><div class="container">
        <div class="flat">
            <div class="flat__left">
                <div class="flat__left__top">
                    <div class="d-flex">
                        <div class="flat__print">Распечатать планировку</div>
                        <a class="flat__print flat__print--no-icon" href="/pdf/?id=${data.id}" target="_blank">Скачать планировку</a>
                    </div>
                    <div class="flat__open-image"></div>
                </div>
                <div class="flat__image"><img alt></div>
                <div class="flat__left__bottom">
                    <div class="flat__left__bottom__item" data-type="furnished">
                    ${images.withoutFurniture || images.functional ? 'Без мебели' : '&nbsp;'}
                    </div>
                    ${images.withoutFurniture ? '<div class="flat__left__bottom__item" data-type="withoutFurniture">С мебелью</div>' : ''}
                    ${images.functional ? '<div class="flat__left__bottom__item" data-type="functional">Функциональная планировка</div>' : ''}
                </div>
                <div class="flat__rose"></div>
            </div>
            <div class="flat__right">
                <div class="flat__status ${data.status}">${getStatus(data.status)}</div>
                <div class="flat__name">Квартира №${data.number}</div>
                <div class="flat__attrs">
                    <div class="flat__attr">
                        <div class="flat__attr__name">Секция</div>
                        <div class="flat__attr__value">${getCurrentSectionName()}</div>
                    </div>
                    <div class="flat__attr">
                        <div class="flat__attr__name">Этаж</div>
                        <div class="flat__attr__value">${data.storey_number}</div>
                    </div>
                    <div class="flat__attr">
                        <div class="flat__attr__name">Комнат</div>
                        <div class="flat__attr__value">${getRooms(data.rooms_number, data.smart_planning)}</div>
                    </div>
                    <div class="flat__attr">
                        <div class="flat__attr__name">Общая, м²</div>
                        <div class="flat__attr__value">${data.total_area}</div>
                    </div>
                    ${data.living_area !== '0.00' ? `
                        <div class="flat__attr">
                            <div class="flat__attr__name">Жилая, м²</div>
                            <div class="flat__attr__value">${data.living_area}</div>
                        </div>
                    ` : ''}
                    ${data.price !== '0.00' ? `
                        <div class="flat__attr">
                          <div class="flat__attr__name">Цена, ₽</div>
                          <div class="flat__attr__value">${numericFormat(+data.price)}</div>
                      </div>
                    ` : ''}
                </div>
                <div class="btn btn-orange" data-modalhandler="book">Забронировать квартиру</div>
            </div>
        </div>
    </div></div>`, {
        baseClass: 'modal-flat',
        touch: false,
        afterLoad: () => {
            window.flat_number = data.number;
            $('.flat__left__bottom__item:eq(0)').trigger('click');
        }
    })
}

function numericFormat(value, decimal, thousand) {
    if (!decimal) decimal = ' ';
    if (!thousand) thousand = '.';
    var parts = value.toString().split('.');
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, decimal) + (parts[1] ? thousand + parts[1] : '');
}

function getStatus(status) {
    switch (status) {
        case 'free': {
            status = 'Свободна';
            break;
        }
        case 'booked': {
            status = 'Забронирована';
            break;
        }
        case 'sold': {
            status = 'Продана';
            break;
        }
    }
    return status;
}

function getRooms(rooms_number, smart_planning) {
    return (rooms_number !== 1 || !smart_planning ? rooms_number : '') + (smart_planning ? 'С' : '')
}

export { openFlat, getStatus, getRooms };