import {
    getStorey,
    getFlats,
    getStoreyCount,
    getDeadlineQuarter,
    getDeadlineYear,
    getFinishing,
    getCurrentSetFlatsStorey,
    setCurrentSectionName
} from './data/select'
import { openFlat, getStatus, getRooms } from '../components/flat';

const miniLabels = {
    // А
    31: {
        'river': 'left',
        'monument': 'top-right',
        'street': 'right'
    },
    // Б
    32: {
        'river': 'left',
        'monument': 'top-right',
        'street': 'right'
    },
    // В
    33: {
      'river': 'bottom',
      'monument': 'top-left',
      'street': 'top'
    },
    // И
    29: {
        'river': 'left',
        'monument': 'top-right',
        'street': 'right'
    },
    // Ж
    28: {
        'river': 'left',
        'monument': 'top-right',
        'street': 'right'
    },
    // Е
    27: {
        'river': 'bottom',
        'monument': 'top-left',
        'street': 'top'
    }
};

const secondBlock = document.querySelector('.select-cols');
const $houses = $('.select-facade__house:not(.select-facade__house--no-hover)');
const $svgContainer = $('.select-floor-plan__svg');
const $flatsContainer = $('.select-chess__flats-container');
const $storeyCount = $('#storey_count')
const $deadlineQuarter = $('#deadline_quarter')
const $deadlineYear = $('#deadline_year')
const $finishing = $('#finishing')
const $number_section = $('.number_section')
const $mini_plan = $('.select-floor-plan__plan .section')

let currentEntrance;
let currentFlat = {};

$flatsContainer.on('click', '.select-chess__line:not(.select-chess__line--active)', function () {
    $flatsContainer.find('.select-chess__line--active').removeClass('select-chess__line--active')
    $flatsContainer.find('.select-chess__floor--active').removeClass('select-chess__floor--active')
    const $self = $(this)
    $self.addClass('select-chess__line--active')
    $self.find('.select-chess__floor').addClass('select-chess__floor--active')
    storeySelect($self.attr('data-number'));
});

let firstClick = true;
$houses.click(function () {
    const $self = $(this);
    $houses.removeClass('select-facade__house--active');
    $self.addClass('select-facade__house--active');
    const sectionName = $self.attr('data-name');
    $number_section.text(sectionName);
    setCurrentSectionName(sectionName);
    sectionSelect($self.attr('data-entrance'));
    if (!firstClick) {
        secondBlock.scrollIntoView();
    }
    firstClick = false;
    $mini_plan.removeClass('active');
    $mini_plan.filter('.section-' + $self.attr('data-name-eng')).addClass('active');
}).eq(0).trigger('click');

function sectionSelect(id) {
    currentEntrance = id;
    const flats = getFlats(id);
    $flatsContainer.html('')
    $storeyCount.text(getStoreyCount())
    $deadlineQuarter.text(getDeadlineQuarter())
    $deadlineYear.text(getDeadlineYear())
    $finishing.text(getFinishing())
    let goodViewCheck = false
    let goodViewAdd = false
    $.each(flats, (key,item) => {
        goodViewCheck = !!item[0].good_view;
        $flatsContainer.prepend(`<div data-number="${key}" class="select-chess__line select-chess__cols${goodViewCheck && !goodViewAdd ? ' select-chess__line--species' : ''}">
            <div class="select-chess__col--floor select-chess__floor">${key}</div>
            <div class="select-chess__col--flats select-chess__flats">
                ${item.map(({status,rooms_number,smart_planning, total_area, storey_number}) => {
                    return `<div class="select-chess__flat ${status}" data-area="${total_area}" data-storey="${storey_number}">${getRooms(rooms_number, smart_planning)}</div>`
                }).join('')}
            </div>
        </div>`);
        if (goodViewCheck && !goodViewAdd) {
            goodViewAdd = true;
        }
    })
    $flatsContainer.find('.select-chess__line').eq(0).click()
}

function storeySelect(number) {
    const data = getStorey(currentEntrance, number);
    if (data) {
        const {flats, image} = data;
        $svgContainer.html(`
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ${image.w} ${image.h}" version="1.1" width="100%" height="100%" preserveAspectRatio="xMidYMid slice">
                <image xlink:href="${cmsTpl}images/sections/${image.href}" width="100%" height="100%" />
                <g>${flats.map(({path,area}) => `<path data-area="${area}" data-storey="${number}" xmlns="http://www.w3.org/2000/svg" d="${path}" />`).join('')}</g>
            </svg>
            <div class="select-floor-plan__river select-floor-plan__river--${miniLabels[currentEntrance].river}"></div>
            <div class="select-floor-plan__monument select-floor-plan__monument--${miniLabels[currentEntrance].monument}"></div>
            <div class="select-floor-plan__street select-floor-plan__street--${miniLabels[currentEntrance].street}">ул. С. Перовской</div>
        `);
    }
}

$svgContainer.on({mousemove:hoverFlat,mouseleave:hoverFlat}, 'path');
let showFlyBlock = false;
let $flyBlock = null;
function hoverFlat(e) {
    if (e.type === 'mousemove') {
        if (!showFlyBlock) {
            const $self = $(this);
            const area = $self.attr('data-area')
            const flatInfo = getCurrentSetFlatsStorey().filter(({total_area}) => total_area === area);
            if (flatInfo.length) {
                currentFlat = flatInfo[0];
            } else {
                currentFlat = {};
            }
            if (!currentFlat.status) {
                console.error('Ошибка! В таблице не найдена площадь ' + area);
            }
            $self.addClass(currentFlat.status);
            $flyBlock = $(currentFlat.status ? `<div class="select-fly-block">
                <div class="select-fly-block__status ${currentFlat.status}">${getStatus(currentFlat.status)}</div>
                <div class="select-fly-block__number">Квартира №${currentFlat.number}</div>
                <div class="select-fly-block__bottom">
                    <div class="select-fly-block__col">
                        <div class="select-fly-block__col__name">Комнат</div>
                        <div class="select-fly-block__col__value">${getRooms(currentFlat.rooms_number, currentFlat.smart_planning)}</div>
                    </div>
                    <div class="select-fly-block__col">
                        <div class="select-fly-block__col__name">Общая, м</div>
                        <div class="select-fly-block__col__value">${area}</div>
                    </div>
                </div>
            </div>` : '');
            $svgContainer.append($flyBlock);
        }
        $flyBlock.css({'top' : e.offsetY, 'left' : e.offsetX});
        showFlyBlock = true;
    } else if (e.type === 'mouseleave') {
        showFlyBlock = false;
        $flyBlock.remove();
        $flyBlock = null;
        currentFlat = null;
    }
}

$flatsContainer.on('touchstart mousedown', () => {
    window.hiddenMenu.disableSwipe();
});
$flatsContainer.on('touchend mouseup', () => {
    window.hiddenMenu.enableSwipe();
});

$svgContainer.on('click', 'path:not(.sold)', openFlat);
$flatsContainer.on('click', '.select-chess__flat:not(.sold)', openFlat);