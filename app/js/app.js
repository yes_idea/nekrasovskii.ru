import './components/formhandler';
import './components/modalhandler/modalhandler';
import './components/hidden-menu';

if (typeof pageName !== 'undefined') {
    try {
        require(`./pages/${pageName}`);
    } catch(e) {
        if (e.message.indexOf('Cannot find module') < 0) {
            console.error(e);
        }
    }
}

$('[target="_blank"]').attr('rel','noopener noreferrer');

$('[type="tel"]').inputmask({
    'mask': '+7 999 999-99-99',
    'onincomplete': e => {
        e.target.value = '';
    }
});