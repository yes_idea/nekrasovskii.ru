const defaultType = 'callme'

$(document).on('click', '[data-modalhandler]', function () {
    let data
    const modalType = $(this).attr('data-modalhandler') || defaultType;
    try {
        data = require('./data/' + modalType).default
    } catch (e) {
        data = require('./data/' + defaultType).default
    }
    $.fancybox.open(data.html, {
        baseClass: 'modalhandler',
        touch: false,
        afterLoad: () => {
            $('[type="tel"]').inputmask({
                'mask': '+7 999 999-99-99',
                'onincomplete': e => {
                    e.target.value = '';
                }
            });
            if (data.afterRender) data.afterRender();
        }
    })
});