import './data/about-flats';

$.getScript('//maps.googleapis.com/maps/api/js?key=AIzaSyBoDsMME1U_GJS8LsvHOaoEQEJk9q01obw', () => {
    const map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(54.70774399640301, 55.992476889947596),
        zoom: 14,
        disableDefaultUI: true,
        gestureHandling: 'cooperative',
        styles: require('./data/style.json')
    });

    new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(54.700447, 55.978797),
        icon: {
            url: `${cmsTpl}images/logo-map.svg`,
            scaledSize: new google.maps.Size(91, 58),
            anchor: { x: 75, y: 43 },
        }
    });

    const MarkerWithLabel = require('markerwithlabel')(google.maps);

    google.maps.event.addListener(map, 'rightclick', function(event) {
        const lat = event.latLng.lat();
        const lng = event.latLng.lng();
        console.log(lat, lng);
    });

    const markers = [];
    map.addListener('click', function () {
        markers.forEach(function (marker) {
            marker.infowindow.close();
        });
    });
    require('./data/points').default.forEach(function (item, i) {
        markers[i] = new MarkerWithLabel({
            map: map,
            position: new google.maps.LatLng(item.position.lat, item.position.lng),
            icon: {url: `${cmsTpl}images/points/${item.category}.svg`},
            labelContent: `<div class="about-map__point__wrap">${item.name}</div>`,
            labelClass: 'about-map__point',
        });

        // открытие балуна
        markers[i].infowindow = new google.maps.InfoWindow({
            content: `<strong>${item.name}</strong> <br>${item.address}`,
            pixelOffset: new google.maps.Size(3, 0)
        });
        markers[i].category = item.category;
        if(item.open) {
            markers[i].infowindow.open(map, markers[i]);
        }
        markers[i].addListener('click', function () {
            if (this.infowindow.anchor) {
                this.infowindow.close();
            } else {
                markers.forEach(function ($marker) {
                    $marker.infowindow.close();
                });
                this.infowindow.open(map, this);
            }
        });
        markers[i].setZIndex(1);
        markers[i].addListener('mouseover', function () {
            markers[i].setZIndex(2);
            /*if (item.name) {
                markers[i].set('labelContent', `<div>${item.name}</div>`)
            }*/
        });
        markers[i].addListener('mouseout', function () {
            //markers[i].set('labelContent', '');
            markers[i].setZIndex(1);
        });
    });

    const $categories = $('.about-map__category');
    $categories.click(function () {
        const $this = $(this);
        $categories.removeClass('about-map__category--active');
        $this.addClass('about-map__category--active');
        Object.values(markers).forEach(item => {
            item.setVisible(item.category === $this.attr('data-name'));
        })
    }).eq(0).trigger('click');
});