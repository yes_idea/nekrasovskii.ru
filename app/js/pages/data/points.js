export default [
    {
        category:'schools',
        name:'Детский сад №292',
        address:'ул. Авроры, 13',
        position: {lat: 54.69924, lng: 56.00566}
    },
    {
        category:'schools',
        name:'Детский сад № 293',
        address:'ул. Генерала Горбатова, 7/2',
        position: {lat: 54.71734, lng: 56.01297}
    },
    {
        category:'schools',
        name:'Частный детский сад <br>«Жемчужинка»',
        address:'ул. Софьи Перовской, 50/3',
        position: {lat: 54.69631, lng: 55.98858}
    },
    {
        category:'schools',
        name:'Детский сад №35',
        address:'ул. Менделеева, 140/3',
        position: {lat: 54.777361, lng: 56.01219}
    },
    {
        category:'schools',
        name:'Детский сад № 3',
        address:'л. Степана Кувыкина, 33/1',
        position: {lat: 54.70916, lng: 55.99658}
    },
    {
        category:'schools',
        name:'Детский сад №23',
        address:'ул. Рабкоров, 3/1',
        position: {lat: 54.70442, lng: 56.0002}
    },
    {
        category:'schools',
        name:'МБОУ Школа № 22',
        address:'ул. Степана Кувыкина, 5/2',
        position: {lat: 54.7001, lng: 55.9914}
    },
    {
        category:'schools',
        name:'Школа № 9 средняя',
        address:'ул. Мубарякова, 14',
        position: {lat: 54.69804, lng: 55.9987}
    },
    {
        category:'schools',
        name:'Школа № 18',
        address:'ул. Мубарякова, 20/1',
        position: {lat: 54.69942, lng: 56.00156}
    },
    {
        category:'schools',
        name:'Открытая (сменная) <br>школа № 142',
        address:'ул. Менделеева, 41',
        position: {lat: 54.71126, lng: 55.97777}
    },
    {
        category:'schools',
        name:'Детская музыкальная <br>школа № 1 им. Н. Г. Сабитова',
        address:'ул. Менделеева, 7',
        position: {lat: 54.71116, lng: 55.97276}
    },
    {
        category:'schools',
        name:'ДЮСШ № 14',
        address:'Дуванский б-р, 22/2',
        position: {lat: 54.70739, lng: 55.99471}
    },
    {
        category:'shopping',
        name:'ТРК Иремель',
        address:'ул. Менделеева, 137',
        position: {lat: 54.71343, lng: 55.99463}
    },
    {
        category:'shopping',
        name:'ТКЦ ULTRA',
        address:'Бакалинская ул., 27',
        position: {lat: 54.71729, lng: 55.98323}
    },
    {
        category:'shopping',
        name:'Шатлык Люкс',
        address:'ул. Степана Кувыкина, 18',
        position: {lat: 54.70777, lng: 55.99963}
    },
    {
        category:'shopping',
        name:'Перовский рынок',
        address:'ул. Софьи Перовской, 17/1',
        position: {lat: 54.69804, lng: 55.99293}
    },
    {
        category:'shopping',
        name:'Ярмарка',
        address:'ул. Академика Ураксина, 1',
        position: {lat: 54.70229, lng: 55.97894}
    },
    {
        category:'shopping',
        name:'Полушка',
        address:'ул. Высотная, 14/1',
        position: {lat: 54.70389, lng: 55.98257}
    },
    {
        category:'shopping',
        name:'Магнит',
        address:'ул. Софьи Перовской, 15',
        position: {lat: 54.6983, lng: 55.99095}
    },
    {
        category:'beauty-and-sports',
        name:'UfaBoxing',
        address:'Бакалинская ул., 60',
        position: {lat: 54.71838, lng: 55.99206}
    },
    {
        category:'beauty-and-sports',
        name:'Yakudza GYM Premium -<br> академия единоборств',
        address:'Бакалинская ул., 25',
        position: {lat: 54.71815, lng: 55.98778}
    },
    {
        category:'beauty-and-sports',
        name:'Спортивная школа<br> № 23',
        address:'ул. Софьи Перовской, 15',
        position: {lat: 54.69872, lng: 55.98979}
    },
    {
        category:'beauty-and-sports',
        name:'Orange Fitness',
        address:'ул. Софьи Перовской, 13/3',
        position: {lat: 54.70163, lng: 55.98808}
    },
    {
        category:'beauty-and-sports',
        name:'Junior Club, <br>Детский Фитнес-центр',
        address:'ул. Менделеева, 137',
        position: {lat: 54.7127, lng: 55.99076}
    },
    {
        category:'beauty-and-sports',
        name:'Фитнес-клуб',
        address:'ул. Степана Кувыкина, 11',
        position: {lat: 54.70251, lng: 55.99506}
    },
    {
        category:'beauty-and-sports',
        name:'Фитнес-центр <br>Mendeleef Fitness',
        address:'ул. Менделеева, 137',
        position: {lat: 54.71328, lng: 55.99335}
    },
    {
        category:'beauty-and-sports',
        name:'EMS-ФИТНЕС',
        address:'ул. Менделеева, д. 128/1',
        position: {lat: 54.71384, lng: 55.99874}
    },
    {
        category:'beauty-and-sports',
        name:'Салон красоты <br>"Тиара"',
        address:'ул. Степана Кувыкина, 5',
        position: {lat: 54.69953, lng: 55.99419}
    },
    {
        category:'beauty-and-sports',
        name:'Бассейн БГПУ <br>им. М. Акмуллы',
        address:'ул. Софьи Перовской, 40',
        position: {lat: 54.69838, lng: 55.98421}
    },
    {
        category:'business',
        name:'Бизнес центр',
        address:'',
        position: {lat: 54.69659, lng: 55.99002}
    },
    {
        category:'business',
        name:'Дело, бизнес-центр',
        address:'Дуванская ул., 18',
        position: {lat: 54.70164, lng: 55.98314}
    },
    {
        category:'business',
        name:'Бизнес Центр Дружба',
        address:'ул. Менделеева, 23',
        position: {lat: 54.71169, lng: 55.98783}
    },
    {
        category:'business',
        name:'УРАЛСИБ',
        address:'ул. Софьи Перовской, дом 15',
        position: {lat: 54.69926, lng: 55.98588}
    },
    {
        category:'business',
        name:'Почта Банк',
        address:'ул. Софьи Перовской, 15',
        position: {lat: 54.69848, lng: 55.99035}
    },
    {
        category:'business',
        name:'Банк ВТБ',
        address:'ул. Софьи Перовской, д. 46',
        position: {lat: 54.69775, lng: 55.98762}
    },
    {
        category:'business',
        name:'СберБанк',
        address:'ул. Софьи Перовской, 44',
        position: {lat: 54.69799, lng: 55.98648}
    },
    {
        category:'business',
        name:'Почта России',
        address:'ул. Загира Исмагилова, 14',
        position: {lat: 54.69771, lng: 55.98221}
    },
    {
        category:'food-and-health',
        name:'Здоровье от природы,<br> магазин',
        address:'ул. Софьи Перовской, 19/2',
        position: {lat: 54.69633, lng: 55.99709}
    },
    {
        category:'food-and-health',
        name:'Детская поликлиника 2',
        address:'ул. Габдуллы Амантая, 7',
        position: {lat: 54.69089, lng: 55.99316}
    },
    {
        category:'food-and-health',
        name:'Центр Ортодонтии<br> Золотое Сечение',
        address:'ул. Загира Исмагилова, 18',
        position: {lat: 54.69882, lng: 55.98027}
    },
    {
        category:'food-and-health',
        name:'Семейная Клиника<br> "Бэхет"',
        address:'ул. Загира Исмагилова, 6',
        size: [50, 50],
        position: {lat: 54.69553, lng: 55.98526}
    },
    {
        category:'food-and-health',
        name:'Санаторий Радуга',
        address:'ул. Авроры, 14/1',
        position: {lat: 54.70357, lng: 56.01248}
    },
    {
        category:'food-and-health',
        name:'Республиканский<br> перинатальный центр',
        address:'ул. Авроры, 16',
        position: {lat: 54.70795, lng: 56.01046}
    },
    {
        category:'food-and-health',
        name:'Больница скорой<br> медицинской помощи',
        address:'ул. Батырская, 39/2',
        position: {lat: 54.71033, lng: 56.00633}
    },
    {
        category:'food-and-health',
        name:'ГБУЗ Республиканский<br> кардиологический центр',
        address:'ул. Степана Кувыкина, 96',
        position: {lat: 54.71114, lng: 56.00189}
    },
    {
        category:'food-and-health',
        name:'РДКБ',
        address:'ул. Степана Кувыкина, 98',
        position: {lat: 54.71229, lng: 56.00631}
    },
    {
        category:'food-and-health',
        name:'Клиника современной<br> флебологии',
        address:'ул. Менделеева, 128/1',
        position: {lat: 54.71368, lng: 56.00066}
    },
    {
        category:'universities',
        name:'Современная гуманитарная академия',
        address:'ул. Карла Маркса, 3Б',
        position: {lat: 54.71988, lng: 55.9374}
    },
    {
        category:'universities',
        name:'УГАТУ',
        address:'ул. Акназарова, 24',
        position: {lat: 54.71688, lng: 55.99724}
    },
    {
        category:'universities',
        name:'Современная гуманитарная академия',
        address:'ул. Карла Маркса, 3',
        position: {lat: 54.72339, lng: 55.94148}
    },
    {
        category:'universities',
        name:'Российский Экономический Университет им. Г. В. Плеханова',
        address:'ул. Менделеева, 177/3',
        position: {lat: 54.72196, lng: 56.00666}
    },
    {
        category:'universities',
        name:'Башкирский государственный университет',
        address:'ул. Заки Валиди, 32',
        position: {lat: 54.72012, lng: 55.93294}
    },
    {
        category:'universities',
        name:'УГАТУ',
        address:'ул. Карла Маркса, 12',
        position: {lat: 54.72497, lng: 55.94246}
    }
];