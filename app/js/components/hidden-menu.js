import MobileSwipeMenu from 'mobile-swipe-menu';
import isMobile from './isMobile';

const shadow = $('.menu-shadow');
const hamburger = $('.header__hamburger');
const menuOpened = () => {
    hamburger.addClass('header__hamburger--opened');
    shadow.css({
        transition: 'opacity .15s ease-in-out',
        visibility: 'visible',
        opacity: 1
    });
};
const menuClosed = () => {
    hamburger.removeClass('header__hamburger--opened');
    shadow.css({
        transition: 'opacity .15s ease-in-out, visibility .15s ease-in-out',
        opacity: 0
    });
    shadow.css('visibility', 'hidden');
};
const menuDrag = ({xCurrent, currentDirection}) => {
    if (currentDirection !== 'left' && currentDirection !== 'right') {
        return false
    }
    if (xCurrent <= 0) {
        const onePercent = innerWidth / 100
        const opacity = Math.round((-xCurrent / onePercent)) / 100;
        shadow.css({
            transition: '',
            visibility: 'visible',
            opacity: opacity > 1 ? 1 : opacity < 0 ? 0 : opacity
        });
    } else {
        shadow.css('visibility', 'hidden');
    }
}


window.hiddenMenu = new MobileSwipeMenu('.hidden-menu', {
    mode: 'right',
    hookWidth: 0,
    enableBodyHook: isMobile,
    width: isMobile ? window.innerWidth / 1.15 : 395,
    events: {
        opened: menuOpened,
        closed: menuClosed,
        drag: menuDrag
    }
});
hamburger.click(() => {
    window.hiddenMenu.openMenu();
});

$('.hidden-menu__close,.menu-shadow').click(() => {
    window.hiddenMenu.closeMenu();
});