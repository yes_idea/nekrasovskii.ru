import isMobile from '../components/isMobile';

const $ss = $('.screen-swiper');
if (!isMobile) {
    new Swiper($ss[0], {
        speed: 1000,
        simulateTouch: false,
        direction: 'vertical',
        spaceBetween: 0,
        mousewheel: true,
        pagination: {
            el: '.screen-pagination',
            clickable: true,
            bulletClass: 'screen-pagination__bullet',
            bulletActiveClass: 'screen-pagination__bullet--active'
        },
    });
} else {
    $ss.css('height', 'auto')
    $('.swiper-wrapper', $ss).css('display', 'block');
}

// const specialBanner = sessionStorage.getItem('banner');
// if (!specialBanner) {
//     $.fancybox.open(
//         '<div class="special-banner">' +
//             '<a class="special-banner__btn" href="javascript:void(0)" data-modalhandler="callme">Получить индивидуальныю цену</a>' +
//             '<img class="special-banner__image" alt src="/images/banner-nekras-4.png" />' +
//         '</div>'
//     );
//     sessionStorage.setItem('banner', '1');
// }
// if (location.hash === '#special') {
//
// }