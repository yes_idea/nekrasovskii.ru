<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

header('Access-Control-Allow-Origin: *');

$server = 'https://app-service.xn--80ahdxelyn.xn--p1ai';

require_once('vendor/autoload.php');

$id = filter_input(INPUT_GET, 'id');

if (empty($id)) die;

$curl = new \Curl\Curl();
/**
 * Получаем инфу квартиры
 */
$curl->get("{$server}/api/v2/dwellings/flats/{$id}");
if ($curl->error) {
    echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
    return;
} else {
    $data = $curl->response->data;
}

$dataSection = require_once 'data.php';
$dataSection = array_filter($dataSection, function ($item) use ($data) {
    return $item['entrances_id'] === $data->entrance;
});
$dataSection = count($dataSection) ? end($dataSection) : [];

$path = '';
if (count($dataSection)) {
    $flats = array_filter($dataSection['flats'], function ($item) use ($data) {
        return $item['area'] == $data->total_area;
    });
    if (count($flats)) {
        $flats = end($flats);
        $path = $flats['path'];
    }
}

$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];
$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];
$mpdf = new Mpdf\Mpdf([
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/assets/fonts',
    ]),
    'fontdata' => $fontData + [
            'Intro' => [
                'R' => 'Intro-Regular.ttf'
            ],
            'TTFirs' => [
                'R' => 'TTFirs-Bold.ttf',
                'B' => 'TTFirs-ExtraBold.ttf'
            ]
        ],
    'default_font' => 'TTFirs'
]);

$mpdf->SetHTMLHeader(render('header'));
$mpdf->WriteHTML(file_get_contents(__DIR__ . '/assets/style.css'), 1);
$mpdf->WriteHTML(render('main', [
    'number' => $data->number,
    'rooms_number' => ($data->rooms_number == 1 ? ($data->smart_planning ? '' : 1) : $data->rooms_number) . ($data->smart_planning ? 'С' : ''),
    'section' => getSection($data->entrance),
    'storey' => $data->storey_number,
    'area' => $data->total_area,
    'price' => number_format($data->price, 0, '', ' '),
    'image_furnished' => $data->images[0],
    'image_without_furniture' => $data->images[1] ?? '',
    'plan' => $dataSection['image'],
    'path' => $path
]));
$mpdf->Output();

function getSection(Int $id): string
{
    $array = [29 => 'И', 28 => 'Ж', 27 => 'Е', 31 => 'А', 32 => 'Б', 33 => 'В'];
    return $array[$id];
}

function render($name, array $data = []): string
{
    ob_start();
    extract($data);
    include __DIR__ . "/views/{$name}.php";
    return ob_get_clean();
}
