import {getAboutFlats} from './select';
import {getRooms} from '../../components/flat';

let result = [];
let section_name = '';
let area = '';
let cart_name = '';
let currentImageType = '';
let data = '';
const images = {
    furnished: '',
    withoutFurniture: '',
    functional: ''
};

$('.flat-mini__view').click(function () {
    const $self = $(this);
    const entrance = Number($self.attr('data-entrance'));
    area = $self.attr('data-area');
    section_name = getSectionName(entrance);
    result = getAboutFlats(entrance, area);
    images.furnished = result.flats[0].images[0];
    images.withoutFurniture = result.flats[0].images[1];
    images.functional = result.flats[0].functional_plan;
    cart_name = $self.parents('.flat-mini').find('.flat-mini__title').html();
    console.log($self.parents('.flat-mini'))
    openFlat();
});

$(document).on('click', '.flat__left__bottom__item--custom', function () {
    $('.flat__left__bottom__item--active').removeClass('flat__left__bottom__item--active');
    const $self = $(this);
    $self.addClass('flat__left__bottom__item--active');
    const imageType = $self.attr('data-type');
    currentImageType = imageType;
    const $imgContainer = $('.flat__image');
    $('img', $imgContainer).attr('src', images[imageType]);
    if (imageType === 'functional') {
        $imgContainer.prepend(`<a class="btn btn-orange" href="${images[imageType]}" download>Скачать планировку</a>`);
    } else {
        $('.btn', $imgContainer).remove();
    }
});

$(document).on('click', '.flat__print--custom', () => {
    window.printJS({
        printable: images[currentImageType],
        type: 'image',
        imageStyle: 'max-width:100%;max-height:calc(80vh - 30px - 20px - 25px);',
        header: `<div style="height: 20vh; margin-bottom: 30px;">
            <div style="font-family: 'Intro', sans-serif; margin-bottom: 20px; font-size: 24px">${cart_name}</div>
            <div style="display: flex; justify-content: space-between; flex-wrap: wrap;">
                <div style="width: 50%; flex-shrink: 0; font-family: 'Roboto', sans-serif; margin-bottom: 5px; font-size: 18px">Секция ${section_name}</div>
                <div style="width: 50%; flex-shrink: 0; font-family: 'Roboto', sans-serif; margin-bottom: 5px; font-size: 18px">Этаж ${data.storey_number}</div>
                <div style="width: 50%; flex-shrink: 0; font-family: 'Roboto', sans-serif; margin-bottom: 5px; font-size: 18px">Комнат ${getRooms(data.rooms_number, data.smart_planning)}</div>
                <div style="width: 50%; flex-shrink: 0; font-family: 'Roboto', sans-serif; margin-bottom: 5px; font-size: 18px">Общая, м² ${data.total_area}</div>
            </div>
            <div style="width: 50%; flex-shrink: 0; font-family: 'Intro', sans-serif; margin-top: 15px; font-size: 18px">Цена, ₽ ${(+data.price).toLocaleString('ru-RU',{minimumFractionDigits:0})}</div>
        </div>
        `,
    });
});

function openFlat() {
    data = result.flats[0];
    $.fancybox.open(`<div><div class="container">
        <div class="flat">
            <div class="flat__left">
                <div class="flat__left__top">
                    <div class="flat__print flat__print--custom">Распечатать планировку</div>
                    <div class="flat__open-image"></div>
                </div>
                <div class="flat__image"><img alt></div>
                <div class="flat__left__bottom">
                    <div class="flat__left__bottom__item flat__left__bottom__item--custom" data-type="furnished">Без мебели</div>
                    <div class="flat__left__bottom__item flat__left__bottom__item--custom" data-type="withoutFurniture">С мебелью</div>
                    ${images.functional ? `<div class="flat__left__bottom__item flat__left__bottom__item--custom" data-type="functional">Функциональная планировка</div>` : ''}
                </div>
                <div class="flat__rose"></div>
            </div>
            <div class="flat__right">
                <div class="flat__status star">Планирвока-бестселлер</div>
                <div class="flat__name">${cart_name}</div>
                <div class="flat__attrs">
                    <div class="flat__attr">
                        <div class="flat__attr__name">Свободных</div>
                        <div class="flat__attr__value">${result.freeFlats.length}</div>
                    </div>
                    <div class="flat__attr">
                        <div class="flat__attr__name">Секция</div>
                        <div class="flat__attr__value">${section_name}</div>
                    </div>
                    <div class="flat__attr">
                        <div class="flat__attr__name">Комнат</div>
                        <div class="flat__attr__value">${getRooms(data.rooms_number, data.smart_planning)}</div>
                    </div>
                    <div class="flat__attr">
                        <div class="flat__attr__name">Общая, м²</div>
                        <div class="flat__attr__value">${data.total_area}</div>
                    </div>
                    ${data.living_area !== '0.00' ? `
                        <div class="flat__attr">
                            <div class="flat__attr__name">Жилая, м²</div>
                            <div class="flat__attr__value">${data.living_area}</div>
                        </div>
                    ` : ''}
                    <div class="flat__attr">
                        <div class="flat__attr__name">Цена, ₽</div>
                        <div class="flat__attr__value">${(+data.price).toLocaleString('ru-RU',{minimumFractionDigits:0})}</div>
                    </div>
                </div>
                ${result.freeFlats.length ? '<div class="btn btn-orange" data-modalhandler="book">Забронировать квартиру</div>' : ''}                
            </div>
        </div>
    </div></div>`, {
        baseClass: 'modal-flat',
        touch: false,
        afterLoad: () => {
            window.flat_number = data.number;
            $('.flat__left__bottom__item:eq(0)').trigger('click');
        }
    });
}

function getSectionName(id) {
    switch (id) {
        case 29: {
            return 'И';
        }
        case 28: {
            return 'Ж';
        }
        case 27: {
            return 'Е';
        }
    }
}