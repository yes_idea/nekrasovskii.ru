import isMobile from '../components/isMobile';
import ajax from './../components/ajax';

const result = ajax('galleries/2', 'gallery').images;

const $ss = $('.gallery-swiper .swiper-container');
const array = result.map(({url}) =>
    `<div class="swiper-slide"><div class="gallery-swiper__item" style="background-image:url(${url})"></div></div>`
);

if (!isMobile) {
    const swiper = new Swiper($ss[0], {
        speed: 1000,
        spaceBetween: 0,
        mousewheel: true,
        navigation: {
            nextEl: '.gallery-swiper__next',
            prevEl: '.gallery-swiper__prev',
        },
        on: {
            init: slideChange, slideChange
        }
    });
    swiper.appendSlide(array);
    $('#total').text(num(swiper.slides.length));
    function slideChange() {
        $('#current').text(num(this.realIndex + 1));
    }
} else {
    $ss.css('height', 'auto')
    $('.swiper-wrapper', $ss).css('display', 'block').html(array.join(''));
    $('.gallery-swiper__prev, .gallery-swiper__next').remove();
}

function num(num) {
    return String(num).length === 1 ? '0' + num : num;
}