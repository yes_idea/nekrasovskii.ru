export default (url, cache_key) => {
    let result = sessionStorage.getItem(cache_key);
    if (!result) {
        $.ajax('https://app-service.xn--80ahdxelyn.xn--p1ai/api/v2/' + url, {
            async: false,
            success: ({data}) => {
                result = data;
                sessionStorage.setItem(cache_key, JSON.stringify(result));
            }
        });
    } else {
        result = JSON.parse(result);
    }
    return result;
}