import ajax from "../components/ajax";

const result = ajax('anonymouses/articles?categoryId=1', 'news')

let date = '';
let title = '';
let desc = '';
let image = '';

$('.news').html(result.map(item => `<div class="news__item" data-id="${item.id}">
  <div class="news__date">${item.place}</div>
  <div class="news__title">${item.name}</div>
  <div class="news__introtext">${item.preview.replace(/<\/?[^>]+(>|$)/g, "")}</div>
  <div class="news__view">
    <svg class="icon icon-arrow-small" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg">
      <use xlink:href="/images/svg/sprite.svg#arrow-small"></use>
    </svg>
  </div>
</div>`).join(''));

const html = () => `<div class="news-popup">
    <div class="news__container">
        <div class="container">
            <div class="news-popup__wrap">
                <div class="news-popup__top">
                    <div class="news-popup__date">${date}</div>
                    <a class="news-popup__close" href="javascript:$.fancybox.close()">закрыть</a>    
                </div>
                <div class="news-popup__content">
                    <div class="news-popup__left">
                        <div class="news-popup__title">${title}</div>
                        <div class="news-popup__desc">${desc}</div>
                    </div>
                    ${image ? `<div class="news-popup__right">
                        <img alt src="${image}">
                    </div>` : ''}
                </div>
            </div>
        </div>
    </div>
</div>`;

$('.news__item').click(function () {
    const news = result.filter(({id}) => id === Number($(this).attr('data-id')))[0];
    date = news.place;
    title = news.name;
    desc = news.preview;
    image = news.previewImageUrl;
    $.fancybox.open(html(), {
        baseClass: 'news-fancy'
    });
});