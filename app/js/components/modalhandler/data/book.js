export default {
    html: `<form class="modalhandler__form" novalidate data-formhandler="Консультация">
        <div class="modalhandler__title">Оставить заявку</div>
        <div class="modalhandler__desc">Оставьте заявку и мы перезвоним вам<br> в ближайшее время</div>
        <label class="modalhandler__group">
            <div class="modalhandler__label">Ваше имя</div>
            <div class="modalhandler__wrap-field modalhandler__wrap-field--white">
                <input class="modalhandler__field modalhandler__field--white form-control" type="text" name="name" data-name="Имя" data-error="Как к Вам обращаться?" required>
            </div>
        </label>
        <label class="modalhandler__group">
            <div class="modalhandler__label">Телефон</div>
            <div class="modalhandler__wrap-field modalhandler__wrap-field--white">
                <input class="modalhandler__field modalhandler__field--white form-control" type="tel" name="phone" data-name="Телефон" data-error="Укажите телефон для связи" required>
            </div>
        </label>
        <input type="hidden" name="flat_number" data-name="Номер квартиры" value="${window.flat_number}">
        <button class="modalhandler__btn btn btn-orange">Отправить</button>
    </form>`
};